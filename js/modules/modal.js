function openModal(modalSelector, modalTimer) {
	modal = document.querySelector(modalSelector);

	modal.classList.add('show');
	modal.classList.remove('hide');
	document.body.style.overflow = 'hidden';

	if (modalTimer) {
		clearInterval(modalTimer);
	}
}

function closeModal (modalSelector) {
	modal = document.querySelector(modalSelector);

	modal.classList.add('hide');
	modal.classList.remove('show');
	document.body.style.overflow = '';
}

function modal(triggerSelector, modalSelector, modalTimer) {

	const dataModal = document.querySelectorAll(triggerSelector),
		  modal = document.querySelector(modalSelector);

	// dataClose = document.querySelector('[data-close]'),

	/*Open*/

	dataModal.forEach((btn) => {
		btn.addEventListener('click', () => openModal(modalSelector, modalTimer));
	});

	window.addEventListener('scroll', showModalByScroll);

	function showModalByScroll() {
		if (window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight) {
			openModal(modalSelector, modalTimer);
			window.removeEventListener("scroll", showModalByScroll);
		}
	}

	/*Close*/

	// dataClose.addEventListener('click', closeModal);

	modal.addEventListener('click', (e) => {
		if (e.target === modal || e.target.getAttribute('data-close') == '') {
			closeModal(modalSelector);
		}
	});

	document.addEventListener('keydown', (e) => {
		if (e.key === 'Escape' && modal.classList.contains('show')) {
			closeModal(modalSelector);
		}
	});

}

export default modal;
export {openModal, closeModal};