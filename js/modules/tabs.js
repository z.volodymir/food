function tabs(tabsParentSelector, tabsSelector, tabsContentSelector, tabsActiveClass) {

	const tabsParent = document.querySelector(tabsParentSelector),
		  tabs = document.querySelectorAll(tabsSelector),
		  tabsContent = document.querySelectorAll(tabsContentSelector);

	function hideTab() {
		tabsContent.forEach((content) => {
			content.classList.add('hide');
			content.classList.remove('show', 'fade');
		})
		tabs.forEach((tab) => {
			tab.classList.remove(tabsActiveClass);
		})
	}

	function showTab(i = 0) {
		tabsContent[i].classList.add('show', 'fade');
		tabsContent[i].classList.remove('hide');

		tabs[i].classList.add(tabsActiveClass);
	}

	hideTab();
	showTab();

	tabsParent.addEventListener('click', (event) => {
		const target = event.target;

		if (target && target.classList.contains(tabsSelector.slice(1))) {
			tabs.forEach((tab, i) => {
				if (tab === target) {
					hideTab();
					showTab(i);
				}
			})
		}
	})

}

export default tabs;