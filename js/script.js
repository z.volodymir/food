// require('es6-promise').polyfill();
import 'nodelist-foreach-polyfill';
import 'jquery';

import tabs from './modules/tabs';
import timer from './modules/timer';
import forms from './modules/forms';
import modal from './modules/modal';
import cards from './modules/cards';
import calc from './modules/calculator';
import slider from './modules/slider';
import { openModal } from './modules/modal';

window.addEventListener('DOMContentLoaded', () => {
  const modalTimer = setTimeout(() => openModal('.modal', modalTimer), 50000);

  tabs(
    '.tabheader__items',
    '.tabheader__item',
    '.tabcontent',
    'tabheader__item_active'
  );
  timer('.timer', '2025-01-01');
  forms('form', modalTimer);
  modal('[data-modal]', '.modal', modalTimer);
  cards();
  calc();
  slider({
    container: '.offer__slider',
    slide: '.offer__slide',
    nextArrow: '.offer__slider-next',
    prevArrow: '.offer__slider-prev',
    totalCounter: '#total',
    currentCounter: '#current',
    wrapper: '.offer__slider-wrapper',
    field: '.offer__slider-inner',
  });
});
